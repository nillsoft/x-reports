<?php


namespace Nillsoft\Reports\Impl;

use Nillsoft\Reports\Api\IReportBuilder;
use Nillsoft\Reports\Api\IReportBuilderFactory;
use Nillsoft\Reports\Api\ReportBuilderException;

class ReportBuilderFactory implements IReportBuilderFactory {

    /**
     * ReportGenerator constructor.
     */
    public function __construct() {
    }

    /**
     * Creates a report from a data source
     * @param string $type
     * @return IReportBuilder
     * @throws ReportBuilderException
     */
    public function create($type) {
        if ($type === "excel") {
            return new ExcelReportBuilder();
        } else {
            throw new ReportBuilderException("Report type {$type} is not supported");
        }
    }

    /**
     * @return array
     */
    public function supportedTypes() {
        return ["excel"];
    }

}