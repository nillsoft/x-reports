<?php

namespace Nillsoft\Reports\Impl;

use Nillsoft\Reports\Api\IReportReaderFactory;

class ReportReaderFactory implements IReportReaderFactory {

    public function createReportReaderFromFile($fileName, array $config = []) {
        return ReportReaderExcel::parseFile($fileName, $config);
    }

    public function createReportReaderFromContent($content, array $config = []) {
        return ReportReaderExcel::parseContent($content, $config);
    }
}