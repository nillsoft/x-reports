<?php

namespace Nillsoft\Reports\Impl;

use Closure;
use Nillsoft\Reports\Api\IReportBuilder;
use Nillsoft\Reports\Api\WrongColumnTypeException;

/**
 * Class DefaultColumn
 * @package Nillsoft\Reports\Impl
 */
class DefaultColumn {

    /**
     * @var string name
     */
    private $name;

    /**
     * @var string type
     */
    private $type;

    /**
     * @var string id
     */
    private $id;

    /**
     * @var Closure
     */
    private $styleClosure;

    /**
     * @var string
     */
    private $styleId;

    /**
     * @var string
     */
    private $headerStyleId;

    /**
     * Column constructor.
     * @param string $name
     * @param string $type
     * @param string $id
     * @throws WrongColumnTypeException
     */
    public function __construct($name, $type, $id, $styleIdOrClosure,$headerStyleId) {
        if(in_array($type, [IReportBuilder::TYPE_DATETIME, IReportBuilder::TYPE_DATE, IReportBuilder::TYPE_STRING, IReportBuilder::TYPE_FLOAT, IReportBuilder::TYPE_INT, IReportBuilder::TYPE_PERCENT])){
            $this->name = $name;
            $this->type = $type;
            $this->id = $id;
            if($styleIdOrClosure instanceof Closure) {
                $this->styleClosure = $styleIdOrClosure;
            } else if (is_string($styleIdOrClosure)) {
                $this->styleId = $styleIdOrClosure;
            }
            $this->headerStyleId = $headerStyleId;
        } else {
            throw new WrongColumnTypeException("Column type {$type} not allowed, must be date, int, string, float or boolean");
        }
    }

    /**
     * Returns name of a column
     * @return string columnName
     */
    public function name() {
        return $this->name;
    }

    /**
     * Returns type of column
     * @return string columnType
     */
    public function type() {
        return $this->type;
    }

    /**
     * Returns the id of a column
     * @return string columnId
     */
    public function id() {
        return $this->id;
    }

    public function styleClosure() {
        return $this->styleClosure;
    }

    public function styleId() {
        return $this->styleId;
    }

    public function headerStyleId() {
        return $this->headerStyleId;
    }
}