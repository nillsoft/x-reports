<?php
/**
 * User: niclas
 * Date: 4/12/16
 * Time: 1:13 PM
 */

namespace Nillsoft\Reports\Impl;


use Nillsoft\Reports\Api\IReportStyle;

class StyleImpl implements IReportStyle
{
    private $size;

    private $bold;

    private $color;

    private $backgroundColor;

    public function fontSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function bold($isBold)
    {
        $this->bold = $isBold;
        return $this;
    }

    public function color($color)
    {
        $this->color = $color;
        return $this;
    }

    public function backgroundColor($color)
    {
        $this->backgroundColor = $color;
        return $this;
    }

    public function getBold() {
        return $this->bold;
    }

    public function getColor() {
        return $this->color;
    }

    public function getBackgroundColor() {
        return $this->backgroundColor;
    }

    public function getSize() {
        return $this->size;
    }


}