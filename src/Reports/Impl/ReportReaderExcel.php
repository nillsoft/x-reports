<?php
namespace Nillsoft\Reports\Impl;


use Nillsoft\Reports\Api\IReportReader;
use Nillsoft\Reports\Api\ReportReaderException;
use \PHPExcel;

class ReportReaderExcel implements IReportReader {

    /**
     * @var PHPExcel
     */
    private $excel = null;

    /**
     * @var array
     */
    private $config;

    private function __construct(PHPExcel $excel, array $config = []) {
        $this->excel = $excel;
        $this->config = $config;
    }

    public static function parseFile($fileName, array $config = []) {
        try {
            $listener = isset($config["listener"]) ? $config["listener"] : function ($message) {
            };
            $listener("Parsing Excel $fileName");
            $fileType = \PHPExcel_IOFactory::identify($fileName);
            $objReader = \PHPExcel_IOFactory::createReader($fileType);
            return new ReportReaderExcel($objReader->load($fileName), $config);
        } catch (\Exception $e) {
            throw new ReportReaderException("Could not load excel file", $e);
        }
    }

    public static function parseContent($content, array $config = []) {
        //PHPExcel does not support consuming data. Must first write it to a temp file.
        $listener = isset($config["listener"]) ? $config["listener"] : function ($message) {
        };
        $file = tempnam(sys_get_temp_dir(), 'excel_');
        $listener("Temporary storing Excel file to disk");
        $handle = fopen($file, "w");
        fwrite($handle, $content);
        fclose($handle);
        try {
            return self::parseFile($file, $config);
        } finally {
            unlink($file);
        }
    }

    public final function sheetNames() {
        return $this->excel->getSheetNames();
    }

    public function sheetReader($sheetName = null) {
        try {
            $sheetName = $sheetName ?: $this->sheetNames()[0];
            return new ReportSheetReaderExcel($this->excel->getSheetByName($sheetName), $this->config);
        } catch (\Exception $e) {
            throw new ReportReaderException("Could not find sheet '$sheetName'");
        }
    }
}