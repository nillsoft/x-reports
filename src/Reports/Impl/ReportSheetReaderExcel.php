<?php

namespace Nillsoft\Reports\Impl;


use Nillsoft\Reports\Api\IReportReader;
use Nillsoft\Reports\Api\IReportSheetReader;
use Nillsoft\Reports\Api\ReportReaderException;
use \PHPExcel_Worksheet;

class ReportSheetReaderExcel implements IReportSheetReader {


    private $columnIdByColumnName = null;
    private $rows = [];

    /**
     * @var  PHPExcel_Worksheet
     */
    private $sheet;

    /**
     * @var \Closure
     */
    private $listener;


    public function __construct(PHPExcel_Worksheet $sheet, array $config = []) {
        $this->sheet = $sheet;
        $this->listener = isset($config["listener"]) ? $config["listener"] : function ($message) {
        };
    }

    private function log($message) {
        call_user_func_array($this->listener, [$message]);
    }

    private function init() {
        if ($this->columnIdByColumnName === NULL) {
            $columnIdByColumnName = [];
            $rowIterator = $this->sheet->getRowIterator();
            foreach ($rowIterator as $row) {
                //Should use only $rowIterator->next() but returns NULL. Instead exit after first iteration.
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell) {
                    if (!is_null($cell)) {
                        $columnIdByColumnName[trim(strval($cell->getValue()))] = $cell->getColumn();
                    }
                }
                $this->columnIdByColumnName = $columnIdByColumnName;
                break;
            }
        }
    }

    public function columnNames() {
        $this->init();
        return array_keys($this->columnIdByColumnName);
    }


    public function exportRows(array $columnNameMappings) {
        $this->init();
        $this->log(sprintf("Exporting rows %s in sheet %s", json_encode($columnNameMappings), $this->sheet->getTitle()));
        $columnIdToNewIdMap = [];
        foreach ($columnNameMappings as $name => $id) {
            if ($columnId = isset($this->columnIdByColumnName[$name]) ? $this->columnIdByColumnName[$name] : null) {
                $columnIdToNewIdMap[$columnId] = $id;
            } else {
                throw new ReportReaderException(sprintf("Could not find column '%s' in sheet '%s' ", $name, $this->sheet->getTitle()));
            }
        }

        $rows = [];
        $rowCount = 0;
        $rowIterator = $this->sheet->getRowIterator();
        foreach ($rowIterator as $row) {
            if ($rowCount > 0) {
                $rowData = [];
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                foreach ($cellIterator as $cell) {
                    if (!is_null($cell)) {
                        $columnId = $cell->getColumn();
                        if ($newId = isset($columnIdToNewIdMap[$columnId]) ? $columnIdToNewIdMap[$columnId] : null) {
                            $cellValue = $cell->getValue();
                            if (is_string($cellValue)) {
                                $cellValue = trim($cellValue);
                                $cellValue = strlen($cellValue) ? $cellValue : null;
                            }
                            $rowData[$newId] = $cellValue;
                        }
                    }
                }
                $rows[] = $rowData;
            }
            $rowCount++;
        }
        return $rows;
    }
}