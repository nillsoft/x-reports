<?php
namespace Nillsoft\Reports\Impl;

use DateTime;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use PHPExcel_Style_Fill;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;
use Nillsoft\Reports\Api\IReportBuilder;
use Nillsoft\Reports\Api\ReportBuilderException;

class ExcelReportBuilder extends ReportBuilder {

    const CONTENT_TYPE = "application/vnd.ms-excel";

    const FILE_SUFFIX = "xls";

    /**
     * Report constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @return Report
     * @throws ReportBuilderException
     */
    public function build() {
        if ($this->columns) {
            $objPHPExcel = new PHPExcel();
            $sheet = $objPHPExcel->getActiveSheet();

            $rowNumber = 1;
            if($this->headerVisibility) {
                $i = 0;
                foreach ($this->columns as $column) {
                    $columnId = PHPExcel_Cell::stringFromColumnIndex($i);
                    $colCoordinate = "{$columnId}1";
                    $sheet->setCellValue($colCoordinate, $column->name());
                    if($headerStyleId = $column->headerStyleId()) {
                        $this->applyStyleToCells("{$colCoordinate}:{$colCoordinate}",$sheet,$headerStyleId);
                    }
                    $i++;
                }
                $rowNumber++;
            }

            foreach ($this->rows as $rowData) {
                $row = $rowData[0];
                $metaData = $rowData[1];
                $rowStyleId = $rowData[2];
                $columnLetter = "A";
                if(is_array($row)) {
                    foreach ($this->columns as $column) {
                        $columnId = $column->id();
                        $styleId = null;
                        $cellCoordinate = $columnLetter . $rowNumber;
                        if ($cellValue = isset($row[$columnId]) ? $row[$columnId] : null) {
                            if($cellValue && is_string($cellValue)) {
                                $url = null;
                                $cellValue = preg_replace_callback("/\[(https?:\/\/.*?)\]\s*$/i", function ($matches) use (&$url) {
                                    $url = $matches[1];
                                    return "";
                                }, $cellValue);
                                if ($url) {
                                    $sheet->getCell($cellCoordinate)->getHyperlink()->setUrl($url);
                                }
                            }
                            if($closure = $column->styleClosure()) {
                                $styleId = $closure($cellValue,$columnId,isset($metaData[$columnId]) ? $metaData[$columnId] : null);
                            }
                            $styleId = $styleId ?: ($column->styleId() ?: $rowStyleId);


                            if (($column->type() === IReportBuilder::TYPE_DATE) || ($column->type() === IReportBuilder::TYPE_DATETIME)) {
                                $excelDate = null;
                                if(is_string($cellValue)){
                                    $excelDate = date('Y-m-d-h-i-s', strtotime($cellValue));
                                } elseif (is_int($cellValue)) {
                                    $excelDate = date('Y-m-d-h-i-s', $cellValue);
                                } elseif ($cellValue instanceof DateTime){
                                    $excelDate = $cellValue->format('Y-m-d-h-i-s');
                                }

                                if($excelDate){
                                    $dateParsed = explode("-", $excelDate);
                                    $cellValue = PHPExcel_Shared_Date::FormattedPHPToExcel($dateParsed[0], $dateParsed[1], $dateParsed[2], $dateParsed[3], $dateParsed[4], $dateParsed[5]);
                                    $sheet->getStyle($cellCoordinate)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
                                }


                            } elseif ($column->type() === IReportBuilder::TYPE_INT || $column->type() === IReportBuilder::TYPE_FLOAT || $column->type()===IReportBuilder::TYPE_PERCENT) {
                                //@TODO: You may use IReportBuilder::TYPE_INT (ex) in your comparison to have constants under control.
                                $sheet->getCell($cellCoordinate)->setDataType(PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                if($column->type()===IReportBuilder::TYPE_PERCENT) {
                                    $sheet->getStyle($cellCoordinate)->getNumberFormat()->applyFromArray(
                                        array(
                                            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                                        )
                                    );
                                }
                            } elseif ($column->type() === IReportBuilder::TYPE_STRING) {
                                $sheet->getCell($cellCoordinate)->setDataType(PHPExcel_Cell_DataType::TYPE_STRING);
                            } else {
                                throw new ReportBuilderException("Bad column type {$column->type()}");
                            }
                            $sheet->setCellValue($cellCoordinate, $cellValue);
                            $this->applyStyleToCells($cellCoordinate,$sheet,$styleId);
                        }
                        $columnLetter++;
                    }
                } else if (is_string($row)) {

                    /** @var TextRow $textRow */
                    $cellCoordinate = $columnLetter . $rowNumber;
                    $sheet->setCellValue($cellCoordinate, $row);
                    $sheet->getCell($cellCoordinate)->setDataType(PHPExcel_Cell_DataType::TYPE_STRING);
                    $cellCount = max(count($this->columns),2);
                    $cellCoords = sprintf('A%d:%s%d',$rowNumber,PHPExcel_Cell::stringFromColumnIndex($cellCount-1),$rowNumber);
                    $sheet->mergeCells($cellCoords);
                    $this->applyStyleToCells($cellCoords,$sheet,$rowStyleId);
                }
                $rowNumber++;
            }
            $column = "A";
            for ($i = 0; $i <= count($this->columns); $i++) {
                $sheet->getColumnDimension($column)->setAutoSize(true);
                $column++;
            }
            foreach($sheet->getRowDimensions() as $rd) {
                $rd->setRowHeight(-1);
            }
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_start();
            $objWriter->save('php://output');
            $excelOutput = ob_get_clean();
            return new Report($excelOutput, self::CONTENT_TYPE, self::FILE_SUFFIX);
        } else {
            throw new ReportBuilderException("Report requires at least 1 column");
        }
    }

    /**
     * @param string $cellRange
     * @param PHPExcel_Worksheet $sheet
     * @param $styleId
     * @throws ReportBuilderException
     */
    private function applyStyleToCells($cellRange,$sheet,$styleId) {
        if($style = $this->getStyle($styleId)) {

            $cellStyle = $sheet->getStyle($cellRange);
            if($backGroundColor = $style->getBackgroundColor()) {
                $cellStyle->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $cellStyle->getFill()->getStartColor()->setRGB($backGroundColor);
            }

            if($color = $style->getColor()) {
                $cellStyle->getFont()->getColor()->setRGB($color);
            }
            if(null!== ($isBold = $style->getBold())) {
                $cellStyle->getFont()->setBold($isBold);
            }
            if(null!==($size = $style->getSize())) {
                $cellStyle->getFont()->setSize($size);
            }
        }
    }
}