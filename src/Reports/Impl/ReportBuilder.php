<?php

namespace Nillsoft\Reports\Impl;

use Closure;
use Nillsoft\Reports\Api\ColumnAlreadyExistsException;
use Nillsoft\Reports\Api\IReportBuilder;
use Nillsoft\Reports\Api\IReportStyle;
use Nillsoft\Reports\Api\WrongColumnFormatException;
use Nillsoft\Reports\Api\ReportBuilderException;

abstract class ReportBuilder implements IReportBuilder {

    /**
     * Array of IColumns
     * @var DefaultColumn[]
     */
    protected $columns = [];

    /**
     * Array of rows
     * @var array|string
     */
    protected $rows = [];

    /**
     * @var
     */
    private $stylesById;


    protected $headerVisibility = true;

    /**
     * Report constructor.
     */
    public function __construct() {}

    public final function addColumn($name, $type, $id, $styleIdOrClosure = null,  $headerStyleId=null) {
        if (array_key_exists($id, $this->columns)) {
            throw new ColumnAlreadyExistsException($id);
        } else {
            if ($type && $id) {
                $columnName = trim(strval($name));
                $columnType = trim(strval($type));
                $columnId = trim(strval($id));
                if (strlen($columnType) > 0 && strlen($columnId) > 0) {
                    $this->columns[$columnId] = new DefaultColumn($columnName, $columnType, $columnId, $styleIdOrClosure, $headerStyleId);
                } else {
                    throw new ReportBuilderException("column type and id cannot be an empty string");
                }
            } else {
                throw new ReportBuilderException("Missing type and/or id");
            }
        }
    }


    public final function addRowData($row, array $metaData = null, $rowStyleId=null) {
        if (is_array($row)) {
            $this->rows[] = [$row,$metaData,$rowStyleId];
            return $this;
        } else if(is_object($row)) {
            $this->rows[] = [(array)$row,$metaData,$rowStyleId];
            return $this;
        } else {
            throw new ReportBuilderException("Input is not of type: array");
        }
    }

    public function addRowText($text, $rowStyleId=null)
    {
        if(is_scalar($text)) {
            $this->rows[] = [strval($text),null,$rowStyleId];
            return $this;
        } else if ($text===null) {
            $this->rows[] = ["",null,$rowStyleId];
            return $this;
        } else {
            throw new ReportBuilderException("Bad input for addRowText:" . gettype($text));
        }
    }

    public function addStyle($id)
    {
        $style = new StyleImpl();
        $this->stylesById[$id] = $style;
        return $style;
    }

    public function setDefaultStyle()
    {
        return $this->addStyle(null);
    }

    /**
     * @param null $id
     * @return StyleImpl
     * @throws ReportBuilderException
     */
    protected function getStyle($id=null) {
        if(isset($this->stylesById[$id])) {
            return $this->stylesById[$id];
        } else if ($id===null) {
            return null;
        }
        throw new ReportBuilderException(sprintf("Unknown style '%s'",$id));
    }

    public function setHeaderVisibility($visibility)
    {
        $this->headerVisibility = boolval($visibility);
    }


}