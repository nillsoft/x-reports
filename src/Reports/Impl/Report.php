<?php
namespace Nillsoft\Reports\Impl;

use Nillsoft\Reports\Api\IReport;

class Report implements IReport {

    /**
     * @var
     */
    private $content;

    /**
     * @var
     */
    private $contentType;

    /**
     * @var
     */
    private $fileSuffix;

    /**
     * Report constructor.
     * @param $content
     * @param $contentType
     * @param $fileSuffix
     */
    public function __construct($content, $contentType, $fileSuffix) {
        $this->content = $content;
        $this->contentType = $contentType;
        $this->fileSuffix = $fileSuffix;
    }

    /**
     * Returns report content
     * @return string
     */
    public function content() {
        return $this->content;
    }

    /**
     * Returns content type
     * @return string
     */
    public function contentType() {
        return $this->contentType;
    }

    /**
     * Returns file suffix
     * @return string
     */
    public function fileSuffix() {
        return $this->fileSuffix;
    }

}