<?php

namespace Nillsoft\Reports\Api;
use Closure;


/**
 * Interface IReportBuilder
 * @package Nillsoft\Reports\Api
 *
 *
 * Information on URL support:
 * To make a cell clickable in Excel add the [http(s)://address] to the end of your data for a specific row and column.
 *
 *
 */

interface IReportBuilder {

    const TYPE_DATE = "date";

    const TYPE_DATETIME = "datetime";

    const TYPE_INT = "int";

    const TYPE_STRING = "string";

    const TYPE_FLOAT = "float";

    const TYPE_PERCENT = "percent";


    /**
     * @param string $id the id of the style
     * @return IReportStyle
     */
    public function addStyle($id);

    /**
     * @return IReportStyle
     */
    public function setDefaultStyle();

    public function setHeaderVisibility($visibility);

    /**
     * Add report column
     * @param string $name The value of cell for the column on the first row.
     * @param string $type The type of column (refer to constants that should move to this interface)
     * @param string $id the unique id of the column that should match corresponding row data.
     * @param string $headerStyleId optional style to be used for the header. This style will only be applied on the first header row
     * @styleClosure styleId|Closure optional styleId or closure
     * <code>function($columnValue, $columnId, $metaData) {
     *      return "someStyleId"; //Or null if no style is to be applied
     * }
     * </code>
     */
    public function addColumn($name, $type, $id, $styleIdOrClosure = null, $headerStyleId=null);

    /**
     * Add a row of data to the report as an associative array or object. The value will be rendered in the column with corresponding id
     * @param array|object $data
     * @param array $metaData associative array (key-value) meta data. Meta data, for the column, will be passed as second argument in #addColumn closure
     * @param string $rowStyleId optional style id to use
     * @return IReportBuilder
     */
    public function addRowData($data, array $metaData = null, $rowStyleId=null);

    /**
     * Adds the text spanning over all columns.
     * @param string $text
     * @param string $rowStyleId optional style id to use
     * @return IReportBuilder
     */
    public function addRowText($text, $rowStyleId = null);

    /**
     * Build the report
     * @return IReport
     */
    public function build();

}