<?php
/**
 * User: niclas
 * Date: 4/12/16
 * Time: 12:49 PM
 */

namespace Nillsoft\Reports\Api;


interface IReportStyle
{
    /**
     * Sets the font size
     * @param $size
     * @return IReportStyle
     */
    public function fontSize($size);

    /**
     * Sets style to be bold
     * @return IReportStyle
     */
    public function bold($isBold);


    /**
     * Sets the RGB color of the style in hex
     * @param $rrggbb (HEX values)
     * @return IReportStyle
     */
    public function color($rrggbb);

    /**
     * Sets the RGB background color of the style in hex
     * @param $rrggbb (HEX values)
     * @return IReportStyle
     */
    public function backgroundColor($rrggbb);



}