<?php

namespace Nillsoft\Reports\Api;

/**
 * Interface IReportReader
 * An abstration of a report with multiple sheets of data.
 * @package Nillsoft\Reports\Api
 */
interface IReportReader {

    /**
     * All sheet names in this report
     * @return string[]
     */
    public function sheetNames();

    /**
     * The reader for a specific sheet
     * @param $sheetName|null If null, selects the default first sheet
     * @return IReportSheetReader
     * @throws ReportReaderException if the sheet could not be found.
     */
    public function sheetReader($sheetName = null);
}