<?php

namespace Nillsoft\Reports\Api;

/**
 * Interface IReportReaderFactory
 * $config supports the parameter "listener" that takes a closure function($message){} to inform about progress.
 * @package Nillsoft\Reports\Api
 */
interface IReportReaderFactory {


    /**
     * @param mixed $content content of an Excel file
     * @return IReportReader
     * @throws ReportReaderException
     */
    public function createReportReaderFromContent($content, array $config = []);

    /**
     * @param Excel $fileName file
     * @return IReportReader
     * @throws ReportReaderException
     */
    public function createReportReaderFromFile($fileName, array $config = []);

}