<?php

namespace Nillsoft\Reports\Api;

/**
 * Class ColumnAlreadyExistsException
 * @package Nillsoft\Reports\Api
 */
class ColumnAlreadyExistsException extends ReportBuilderException {

    /**
     * @var int
     */
    private $columnId;

    /**
     * ColumnAlreadyExistsException constructor.
     * @param int $columnId
     * @param \Exception|null $cause
     */
    public function __construct($columnId, \Exception $cause = null) {
        parent::__construct(sprintf("The column %s is already defined", $columnId), $cause);
        $this->columnId = $columnId;
    }

    /**
     * @return null
     */
    public function columnId() {
        return $this->columnId;
    }

}
