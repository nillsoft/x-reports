<?php

namespace Nillsoft\Reports\Api;

/**
 * Class ReportBuilderException
 * @package Nillsoft\Reports\Api
 */
class ReportBuilderException extends \Exception {

    /**
     * ReportBuilderException constructor.
     * @param null $message
     * @param \Exception|null $cause
     */
    public function __construct($message = null, \Exception $cause = null) {
        parent::__construct($message, 0, $cause);
    }
}
