<?php

namespace Nillsoft\Reports\Api;


/**
 * Interface IReport
 * @package Nillsoft\Reports\Api
 */
interface IReport {
    /**
     * Returns report content
     * @return string
     */
    public function content();

    /**
     * Returns content type
     * @return string
     */
    public function contentType();

    /**
     * Returns file suffix
     * @return string
     */
    public function fileSuffix();

}