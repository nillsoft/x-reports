<?php

namespace Nillsoft\Reports\Api;

/**
 * Class WrongColumnTypeException
 * @package Nillsoft\Reports\Api
 */
class WrongColumnTypeException extends \Exception {

    /**
     * WrongColumnTypeException constructor.
     * @param null $message
     * @param \Exception|null $cause
     */
    public function __construct($message = null, \Exception $cause = null) {
        parent::__construct($message, 0, $cause);
    }
}
