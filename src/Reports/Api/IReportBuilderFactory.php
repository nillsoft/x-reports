<?php

namespace Nillsoft\Reports\Api;


/**
 * Interface IReportBuilderFactory
 * @package Nillsoft\Reports\Api
 */
interface IReportBuilderFactory {
    /**
     * Creates a report builder of a specific type, i.e. excel, ...
     * @param string $type
     * @return IReportBuilder
     */
    public function create($type);

    /**
     * Returns the different types of report builders than can be created.
     * @return mixed
     */
    public function supportedTypes();
}