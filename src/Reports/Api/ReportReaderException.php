<?php

namespace Nillsoft\Reports\Api;

/**
 * Class ReportReaderException
 * @package Nillsoft\Reports\Api
 */
class ReportReaderException extends \Exception {

    /**
     * @param $message
     * @param \Exception|null $previous
     */
    public function __constructor($message, \Exception $previous = null) {
        parent::__construct($message, 0, $previous);
    }

}