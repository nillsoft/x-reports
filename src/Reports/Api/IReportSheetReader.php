<?php

namespace Nillsoft\Reports\Api;

/**
 * Interface IReportSheetReader
 * @package Nillsoft\Reports\Api
 */
interface IReportSheetReader {

    /**
     * @return string[] The name of the different columns of this sheet. (Content of cells of the first row).
     */
    public function columnNames();

    /**
     * Exports the content of all cells in all rows, in the sheet, as an 2-level array where each entry in the first level is row of data. The row data is an associative array where column data is mapped by the $columnNameMappings
     * @param array $columnNameMappings an associative array where column names are mapped to array keys for each cell data.
     * <code>
     * $columnNameMappings = [
     *  "Name of Person" => "personName",
     *  "Birth Date" => "birthDate"
     * ]
     *
     * $exportedRows = [
     *  [
     *      "personName" => "Bill",
     *      "birthDate" = > "2000-01-01"
     *  ],
     *  [
     *      "personName" => "Joan",
     *      "birthDate => "2001-01-01"
     *  ]
     * ]
     *
     * </code>
     * @return array
     */
    public function exportRows(array $columnNameMappings);

}