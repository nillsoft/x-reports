<?php

namespace Nillsoft\Reports;

use Nillsoft\Reports\Api\IReportBuilder;
use Nillsoft\Reports\Impl\ReportBuilderFactory;

class GeneralTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateTextLine() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addColumn("Test1",IReportBuilder::TYPE_STRING,"test");
        $reportBuilder->addColumn("Test2",IReportBuilder::TYPE_STRING,"test2");
        $reportBuilder->addColumn("Test3",IReportBuilder::TYPE_STRING,"test3");
        $reportBuilder->addColumn("Test4",IReportBuilder::TYPE_STRING,"test4");
        $reportBuilder->addColumn("Test5",IReportBuilder::TYPE_STRING,"test5");
        $reportBuilder->addRowData(["test"=>"Row1"]);
        $reportBuilder->addRowText("This is the row");
        $reportBuilder->addRowData(["test"=>"Row2"]);

        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test1.xls";
        file_put_contents($file,$content);
    }

    public function testCreateRedText() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addStyle("testStyle")->color("FF0000")->backgroundColor("777777")->bold(true)->fontSize(14);
        $reportBuilder->addColumn("Test",IReportBuilder::TYPE_STRING,"test","testStyle");
        $reportBuilder->addColumn("Test2",IReportBuilder::TYPE_STRING,"test2");

        $reportBuilder->addRowData(["test"=>"This is a test","test2" => "No diff"]);

        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test2.xls";
        file_put_contents($file,$content);
    }

    public function testCreateNegPos() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addStyle("neg")->color("FF0000")->bold(true)->backgroundColor("444444");
        $reportBuilder->addStyle("pos")->color("00FF00");
        $reportBuilder->addColumn("Number",IReportBuilder::TYPE_FLOAT,"number", function($value,$id,$metaData){
            return $value < 0 ? "neg" : "pos";
        });
        for($i = 0;$i<100;$i++) {
            $reportBuilder->addRowData(["number"=>rand(-100,100)]);
        }

        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test3.xls";
        file_put_contents($file,$content);
    }

    public function testCreateLink() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addStyle("colorStyle")->color("ff0000")->backgroundColor("00FFFF");
        $reportBuilder->addColumn("TextCol",IReportBuilder::TYPE_DATE,"text","colorStyle");

        $reportBuilder->addRowData(["text"=>"1998-10-11[http://www.nillsoft.com]"]);

        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test4.xls";
        file_put_contents($file,$content);
    }

    public function testMetaData() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addStyle("neg")->color("FF0000");
        $reportBuilder->addStyle("pos")->color("00FF00");
        $reportBuilder->addColumn("Number",IReportBuilder::TYPE_INT,"number",function($value,$id,$metaData){
            if($metaData === false) {
                return "neg";
            } else {
                return "pos";
            }
        });

        $reportBuilder->addRowData(["number"=>-1],["number"=>false]);
        $reportBuilder->addRowData(["number"=>1],["number"=>true]);


        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test5.xls";
        file_put_contents($file,$content);
    }

    public function testHideHeaders() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addColumn("Number",IReportBuilder::TYPE_INT,"number");
        $reportBuilder->setHeaderVisibility(false);

        $reportBuilder->addRowData(["number"=>-1]);
        $reportBuilder->addRowData(["number"=>1]);


        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test6.xls";
        file_put_contents($file,$content);
    }

    public function testHeaderStyle() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addStyle("red")->color("FF0000");
        $reportBuilder->addColumn("Red",IReportBuilder::TYPE_STRING,"col1",null, "red");
        $reportBuilder->addColumn("Nothing",IReportBuilder::TYPE_STRING,"col2");

        $reportBuilder->addRowData(["col1"=>"Col1","col2"=>"Col2"]);


        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test7.xls";
        file_put_contents($file,$content);
    }

    public function testPercent() {
        $reportBuilderFactory = new ReportBuilderFactory();
        $reportBuilder = $reportBuilderFactory->create("excel");
        $reportBuilder->addColumn("Number",IReportBuilder::TYPE_PERCENT,"number");

        $reportBuilder->addRowData(["number"=>0.1]);
        $reportBuilder->addRowData(["number"=>1]);


        $report = $reportBuilder->build();

        $content = $report->content();
        $this->assertNotNull($content);

        $file = sys_get_temp_dir() . "/test8.xls";
        file_put_contents($file,$content);
    }
}